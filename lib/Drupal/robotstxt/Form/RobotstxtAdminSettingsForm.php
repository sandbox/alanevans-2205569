<?php

/**
 * @file
 * Contains \Drupal\robotstxt\Form\RobotstxtAdminSettingsForm.
 */

namespace Drupal\robotstxt\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Component\Utility\String;

/**
 * Configure robotstxt settings for this site.
 */
class RobotstxtAdminSettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'robotstxt_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $form['robotstxt_content'] = array(
      '#type' => 'textarea',
      '#title' => t('Contents of robots.txt'),
      '#default_value' => _robotstxt_content(),
      '#cols' => 60,
      '#rows' => 20,
      '#wysiwyg' => FALSE,
    );

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $value = String::checkPlain($form_state['values']['robotstxt_content']);
    \Drupal::config('robotstxt.settings')->set('content', $value)->save();
  }
}
