<?php

/**
 * @file
 * Contains \Drupal\robotstxt\RobotstxtController.
 */

namespace Drupal\robotstxt\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides output robots.txt output.
 */
class RobotstxtController {

  /**
   * Serves the configured robots.txt file.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The robots.txt file as a response object with 'text/plain' content type.
   */
  public function content() {
    $content = \Drupal::config('robotstxt.settings')->get('content');
    return new Response($content, 200, array('Content-Type' => 'text/plain'));
  }

}
